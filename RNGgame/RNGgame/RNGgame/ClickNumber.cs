﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNGgame
{
    class ClickNumber
    {
        public int Number;
        public static int Sum;

        public ClickNumber(int _number)
        {
            Number = _number;
        }


        public string GetColour(int number)
        {
            int num;
            string x;

            num = number;

            if (num == Number)
            {
                x = "#51ff5a";
                Sum += 1;
            }
            else
            {
                x = "#ff6e51";
            }

            return x;
        }

        public int GenerateNumber()
        {
            Random random = new Random();
            int rand = random.Next(1, 10);
            return rand;
        }

    }
}
