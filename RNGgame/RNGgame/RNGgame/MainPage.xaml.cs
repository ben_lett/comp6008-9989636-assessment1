﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RNGgame
{
    
    public partial class MainPage : ContentPage
    {
        static string q1 = "Total game wins:";
        static string q2 = "Computers number was:";
        public static string back;

        public MainPage()
        {
            InitializeComponent();
        }

        public void b1Button(object sender, EventArgs e)
        {
            var number = int.Parse(text1.Text);

            var cn = new ClickNumber(number);

            var showRandomNumber = cn.GenerateNumber();
            var getColour = cn.GetColour(showRandomNumber);

            Background.BackgroundColor = Color.FromHex($"{getColour}");

            lable4.Text = (q2 + showRandomNumber);
            lable2.Text = (q1 + ClickNumber.Sum);
            text1.Text = "";
        }

    }

}
